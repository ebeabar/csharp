﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Sea
    {
        public static int SEA_SIZE = 10;
        public static int VECTOR_SIZE = 4;

        public enum CellStatus { Empty, Ship, Dmz };

        private int[,] seaTable = new int[SEA_SIZE, SEA_SIZE];

        private Random rnd = new Random(Guid.NewGuid().GetHashCode());

        private Dictionary<String, List<CoordinateHelper>> coordinateMap = new Dictionary<String, List<CoordinateHelper>>();

        public int[,] SeaTable
        {
            get
            {
                return seaTable;
            }
        }

        public void createSea()
        {
            for (int i = 0; i < SEA_SIZE; i++)
            {
                for (int j = 0; j < SEA_SIZE; j++)
                {
                    seaTable[i, j] = (int)CellStatus.Empty;
                }
            }
        }

        //generate particular ship
        public void generateShipConfiguration()
        {
            generateShip(4, 1);

            generateShip(3, 1);
            generateShip(3, 2);

            generateShip(2, 1);
            generateShip(2, 2);
            generateShip(2, 3);

            generateShip(1, 1);
            generateShip(1, 2);
            generateShip(1, 3);
            generateShip(1, 4);
        }

        //generate all coordinates of ship 
        private void generateShip(int size, int number)
        {
            List<Coordinate> coordinates = new List<Coordinate>(size);
            do
            {
                coordinates = generateCoordinates(size);
            } while (coordinates == null);

            applyShip(coordinates, number);
        }


        //generate coordinates of ship (First Point and Next - maybe imposible, then return null)
        private List<Coordinate> generateCoordinates(int size)
        {
            int[,] copyOfSeaTable = seaTable.Clone() as int[,];

            List<Coordinate> coordinates = new List<Coordinate>(size);
            Coordinate nextPoint;
            Coordinate point = generateFirstPoint(copyOfSeaTable);
            coordinates.Add(point);

            for (int i = 0; i < size - 1; i++)
            {
                int[] vector = generateVector();
                nextPoint = generateNextPoint(vector, point, copyOfSeaTable);
                if (nextPoint == null)
                {
                    return null;
                }
                copyOfSeaTable[nextPoint.x, nextPoint.y] = (int)CellStatus.Ship;
                coordinates.Add(nextPoint);
            }
            return coordinates;
        }

        //generate First Point of ship
        private Coordinate generateFirstPoint(int[,] seaCopy)
        {
            int x, y;

            do
            {
                x = rnd.Next(0, SEA_SIZE);
                y = rnd.Next(0, SEA_SIZE);
            }
            while (!isEmpty(x, y, seaCopy));

            Coordinate point = new Coordinate();
            point.x = x;
            point.y = y;
            seaCopy[point.x, point.y] = (int)CellStatus.Ship;

            return point;
        }

        //genrate Next Points of ship
        private Coordinate generateNextPoint(int[] vector, Coordinate point, int[,] seaCopy)
        {
            for (int i = 0; i < VECTOR_SIZE; i++)
            {
                int x = point.x;
                int y = point.y;
                if (vector[i] == 1)
                {
                    y++;
                }
                else if (vector[i] == 2)
                {
                    y--;
                }
                else if (vector[i] == 3)
                {
                    x++;
                }
                else if (vector[i] == 4)
                {
                    x--;
                }

                if (isInBounds(x, y) && isEmpty(x, y, seaCopy))
                {
                    Coordinate nextPoint = new Coordinate();
                    nextPoint.x = x;
                    nextPoint.y = y;
                    return nextPoint;
                }
            }
            return null;
        }

        //paint Ship and DMZ cells
        public void applyShip(List<Coordinate> coordinates, int number)
        {
            List<CoordinateHelper> coorHelpers = new List<CoordinateHelper>();

            foreach (Coordinate coordinate in coordinates)
            {
                seaTable[coordinate.x, coordinate.y] = (int)CellStatus.Ship;
                CoordinateHelper coorHelper = new CoordinateHelper(coordinate);
                coorHelpers.Add(coorHelper);
            }

            coordinateMap.Add(coordinates.Count + "_" + number, coorHelpers);

            foreach (Coordinate coordinate in coordinates)
            {
                paintDmz(coordinate);
            }
        }

        //paint gray cells
        private void paintDmz(Coordinate coordinate)
        {
            Coordinate shiftCoordinateRight = new Coordinate(coordinate.x, coordinate.y + 1);
            Coordinate shiftCoordinateLeft = new Coordinate(coordinate.x, coordinate.y - 1);
            Coordinate shiftCoordinateUp = new Coordinate(coordinate.x + 1, coordinate.y);
            Coordinate shiftCoordinateDown = new Coordinate(coordinate.x - 1, coordinate.y);
            Coordinate shiftCoordinateUpRight = new Coordinate(coordinate.x + 1, coordinate.y + 1);
            Coordinate shiftCoordinateUpLeft = new Coordinate(coordinate.x + 1, coordinate.y - 1);
            Coordinate shiftCoordinateDownLeft = new Coordinate(coordinate.x - 1, coordinate.y - 1);
            Coordinate shiftCoordinateDownRight = new Coordinate(coordinate.x - 1, coordinate.y + 1);

            List<Coordinate> coordinates = new List<Coordinate>(8);
            coordinates.Add(shiftCoordinateRight);
            coordinates.Add(shiftCoordinateLeft);
            coordinates.Add(shiftCoordinateUp);
            coordinates.Add(shiftCoordinateDown);
            coordinates.Add(shiftCoordinateUpRight);
            coordinates.Add(shiftCoordinateUpLeft);
            coordinates.Add(shiftCoordinateDownLeft);
            coordinates.Add(shiftCoordinateDownRight);

            foreach (Coordinate coord in coordinates)
            {
                if (checkIfExists(coord))
                {
                    seaTable[coord.x, coord.y] = (int)CellStatus.Dmz;
                }
            }
        }

        //check if cell is in the Sea
        public bool isInBounds(int x, int y)
        {
            if ((x < SEA_SIZE && x >= 0) && (y < SEA_SIZE && y >= 0))
            {
                return true;
            }
            return false;
        }

        //check if cell is "white"/empty
        private bool isEmpty(int x, int y, int[,] sea)
        {
            if (sea[x, y] != (int)CellStatus.Ship && sea[x, y] != (int)CellStatus.Dmz)
            {
                return true;
            }
            return false;
        }


        //check if coordinate exist in the Sea
        private bool checkIfExists(Coordinate coordinate)
        {
            if (isInBounds(coordinate.x, coordinate.y) && isEmpty(coordinate.x, coordinate.y, seaTable))
            {
                return true;
            }
            return false;
        }

        // 1 - right, 2 - left, 3- up, 4 - down
        // generate vector which direction we will build ship
        private int[] generateVector()
        {
            int z;
            int[] vector = new int[VECTOR_SIZE];

            for (int i = 0; i < VECTOR_SIZE; i++)
            {
                do
                {
                    z = rnd.Next(VECTOR_SIZE + 1);
                }
                while (vector.Contains(z));
                vector[i] = z;
            }
            return vector;
        }

        public class Coordinate
        {
            public int x;
            public int y;

            public Coordinate() { }

            public Coordinate(int a, int b)
            {
                x = a;
                y = b;
            }

            public override bool Equals(System.Object obj)
            {
                // If parameter is null return false.
                if (obj == null)
                {
                    return false;
                }

                // If parameter cannot be cast to Point return false.
                Coordinate p = obj as Coordinate;
                if ((System.Object)p == null)
                {
                    return false;
                }

                // Return true if the fields match:
                return (x == p.x) && (y == p.y);
            }

            public override int GetHashCode()
            {
                return x ^ y;
            }

        }

        private class CoordinateHelper
        {
            public Coordinate coordinate;
            public bool hit;

            public CoordinateHelper(Coordinate coor)
            {
                coordinate = coor;
                hit = false;
            }
        }

        public class CoordrinateStatusHit
        {
            public List<Coordinate> coordinates;
            public BattleShip.Attempt attempt;

            public CoordrinateStatusHit(BattleShip.Attempt attempt, List<Coordinate> coordinates)
            {
                this.attempt = attempt;
                this.coordinates = coordinates;
            }

            public CoordrinateStatusHit( BattleShip.Attempt attempt)
            {
                this.attempt = attempt;
            }
        }

        public CoordrinateStatusHit checkSeaStatus(int x, int y)
        {
            Coordinate tempCoordinate = new Coordinate(x, y);
            List<Coordinate> coordinatesList = new List<Coordinate>();

            foreach (KeyValuePair<string, List<CoordinateHelper>> entry in coordinateMap)
            {
                coordinatesList.Clear();
                bool areAllCoordinatesHit = true;
                bool hitCoordinate = false;

                foreach (CoordinateHelper coorHelper in entry.Value)
                {
                    coordinatesList.Add(coorHelper.coordinate);
                    if (coorHelper.coordinate.Equals(tempCoordinate))
                    {
                        coorHelper.hit = true;
                        hitCoordinate = true;
                    }

                    if (coorHelper.hit == false)
                    {
                        areAllCoordinatesHit = false;
                    }

                }

                if (hitCoordinate == true)
                {
                    if (areAllCoordinatesHit == true)
                    {
                        return new CoordrinateStatusHit(BattleShip.Attempt.Sank, coordinatesList);
                    }
                    return new CoordrinateStatusHit(BattleShip.Attempt.Hit);
                }

            }
            return new CoordrinateStatusHit(BattleShip.Attempt.Blank);
        }

        public bool checkIfAllShipSank()
        {

            foreach (KeyValuePair<string, List<CoordinateHelper>> entry in coordinateMap)
            {
                foreach (CoordinateHelper coorHelper in entry.Value)
                {
                    if (coorHelper.hit == false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }


    }
}

