﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleShip
{
    public partial class UserCreation : Form
    {
        public UserCreation()
        {
            InitializeComponent();
        }

        private void createUser_Click(object sender, EventArgs e)
        {

            string user = username.Text;
            string passwd = password.Text;
            string repeatedPassword = repeatPassword.Text;
            validation.Text = string.Empty;

            if (user == null || user == string.Empty || user.Length < 4)
            {
                validation.Text = "Username can't be empty or consist less than 4 characters";
                return;
            }

            if (passwd == null || passwd == string.Empty || passwd.Length < 4)
            {
                validation.Text = "Password can't be empty or consist less than 8 characters";
                return;
            }

            if (repeatedPassword == null || repeatedPassword == string.Empty || repeatedPassword.Length < 4)
            {
                validation.Text = "Repeated password can't be empty or consist less than 8 characters";
                return;
            }

            if (!repeatedPassword.Equals(passwd, StringComparison.Ordinal))
            {
                validation.Text = "Passwords don't match";
                return;
            }

            SqlConnection con = new SqlConnection(Properties.Settings.Default.datasource);
            SqlCommand cmd = new SqlCommand("Select * from [batlleShip].[dbo].[user] where UserName=@username", con);
            cmd.Parameters.AddWithValue("@username", user);
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            con.Close();
            int count = ds.Tables[0].Rows.Count;

            if (count == 1)
            {
                validation.Text = "Username already exist. Please choose another user name.";
            }
            else
            {
                con = new SqlConnection(Properties.Settings.Default.datasource);
                cmd = new SqlCommand("Insert into [batlleShip].[dbo].[user] (UserName, Password) Values (@username,@password)", con);
                cmd.Parameters.AddWithValue("@username", user);
                cmd.Parameters.AddWithValue("@password", passwd);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("User created. Please log in.");

                UserForm userForm = new UserForm();
                userForm.Show();
                this.Hide();
            }
        }

        private void helpButtonClicked(Object sender, CancelEventArgs e)
        {
            string text = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"help.txt"));
            System.Windows.Forms.MessageBox.Show(text, "Game Rules");
        }

        private void back_Click(object sender, EventArgs e)
        {
            UserForm userForm = new UserForm();
            userForm.Show();
            this.Hide();
        }

        private void UserCreation_Load(object sender, EventArgs e)
        {

        }
    }

}
