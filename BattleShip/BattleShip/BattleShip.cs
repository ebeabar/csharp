﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class BattleShip
    {
        public Sea player = new Sea();
        public Sea computer = new Sea();
        public int[,] attemptComputer = new int[Sea.SEA_SIZE, Sea.SEA_SIZE];
        public int[,] attemptPlayer = new int[Sea.SEA_SIZE, Sea.SEA_SIZE];
        public enum Player { Player, Computer };
        public enum Attempt { Unknown, Hit, Sank, Blank };
        private Random rnd = new Random(Guid.NewGuid().GetHashCode());
        private Sea.Coordinate computerPreviousMove;
        private string username;

        public void initializeGame(string usr)
        {
            username = usr;
            player.createSea();
            computer.createSea();
            computer.generateShipConfiguration();
        }

        public void reinitializeGame()
        {
            initializeGame(username);
        }

        public Player startGame()
        {
            return randomFirstPlayer();
        }

        public void generateShipConfiguration()
        {
            player.generateShipConfiguration();
        }

        public void applyUserConfig(List<Sea.Coordinate> coordinates, int number)
        {
            player.applyShip(coordinates, number);
        }

        public Player randomFirstPlayer()
        {
            int firstPlayer = rnd.Next(2);

            if (firstPlayer == 0)
            {
                return Player.Player;
            }
            else
            {
                return Player.Computer;
            }
        }

        public ComputerResult computerGame()
        {
            int x = 0, y = 0;
            if (computerPreviousMove == null)
            {
                do
                {
                    x = rnd.Next(Sea.SEA_SIZE);
                    y = rnd.Next(Sea.SEA_SIZE);
                }
                while (!isNotShootBefore(x, y));
            }
            else
            {
                int[] vector = generateVector();
                int count = 0;
                for (int i = 0; i < Sea.VECTOR_SIZE; i++)
                {

                    x = computerPreviousMove.x;
                    y = computerPreviousMove.y;
                    if (vector[i] == 1)
                    {
                        y++;
                    }
                    else if (vector[i] == 2)
                    {
                        y--;
                    }
                    else if (vector[i] == 3)
                    {
                        x++;
                    }
                    else if (vector[i] == 4)
                    {
                        x--;
                    }
                    count++;
                    if (checkIfExists(new Sea.Coordinate(x, y), attemptComputer))
                    {
                        break;
                    }
                    if (count == 4)
                    {
                        computerPreviousMove = null;
                        break;
                    }
                }
            }

            Sea.CoordrinateStatusHit result = player.checkSeaStatus(x, y);

            if (result.attempt == Attempt.Hit)
            {
                computerPreviousMove = new Sea.Coordinate(x, y);
            }
            else if (result.attempt == Attempt.Sank)
            {
                computerPreviousMove = null;
            }

            attemptComputer[x, y] = (int)result.attempt;

            ComputerResult computerResult = new ComputerResult(result.attempt, x, y, result.coordinates);

            return computerResult;
        }

        public Sea.CoordrinateStatusHit playerGame(int x, int y)
        {
            Sea.CoordrinateStatusHit cordinateStatusHit = computer.checkSeaStatus(x, y);
            attemptPlayer[x, y] = (int)cordinateStatusHit.attempt;

            return cordinateStatusHit;

        }

        public List<Sea.Coordinate> greyOutAllAdjacent(int x, int y, bool player)
        {
            int[,] gueses;
            if (player)
            {
                gueses = attemptPlayer;
            }
            else
            {
                gueses = attemptComputer;
            }
            List<Sea.Coordinate> result = new List<Sea.Coordinate>();

            Sea.Coordinate shiftCoordinateRight = new Sea.Coordinate(x, y + 1);
            Sea.Coordinate shiftCoordinateLeft = new Sea.Coordinate(x, y - 1);
            Sea.Coordinate shiftCoordinateUp = new Sea.Coordinate(x + 1, y);
            Sea.Coordinate shiftCoordinateDown = new Sea.Coordinate(x - 1, y);
            Sea.Coordinate shiftCoordinateUpRight = new Sea.Coordinate(x + 1, y + 1);
            Sea.Coordinate shiftCoordinateUpLeft = new Sea.Coordinate(x + 1, y - 1);
            Sea.Coordinate shiftCoordinateDownLeft = new Sea.Coordinate(x - 1, y - 1);
            Sea.Coordinate shiftCoordinateDownRight = new Sea.Coordinate(x - 1, y + 1);

            List<Sea.Coordinate> coordinates = new List<Sea.Coordinate>(8);
            coordinates.Add(shiftCoordinateRight);
            coordinates.Add(shiftCoordinateLeft);
            coordinates.Add(shiftCoordinateUp);
            coordinates.Add(shiftCoordinateDown);
            coordinates.Add(shiftCoordinateUpRight);
            coordinates.Add(shiftCoordinateUpLeft);
            coordinates.Add(shiftCoordinateDownLeft);
            coordinates.Add(shiftCoordinateDownRight);

            foreach (Sea.Coordinate coord in coordinates)
            {
                if (checkIfExists(coord, gueses))
                {
                    result.Add(coord);
                    gueses[coord.x, coord.y] = (int)Attempt.Blank;
                }
            }
            return result;
        }

        private bool isInBounds(int x, int y)
        {
            if ((x < Sea.SEA_SIZE && x >= 0) && (y < Sea.SEA_SIZE && y >= 0))
            {
                return true;
            }
            return false;
        }

        //check if cell is "white"/empty
        private bool isEmpty(int x, int y, int[,] gueses)
        {
            if (gueses[x, y] == (int)Attempt.Unknown)
            {
                return true;
            }
            return false;
        }


        //check if coordinate exist in the Sea
        private bool checkIfExists(Sea.Coordinate coordinate, int[,] guesses)
        {
            if (isInBounds(coordinate.x, coordinate.y) && isEmpty(coordinate.x, coordinate.y, guesses))
            {
                return true;
            }
            return false;
        }

        private bool isNotShootBefore(int x, int y)
        {
            return (attemptComputer[x, y] == (int)Attempt.Unknown);
        }

        public class ComputerResult
        {
            public Attempt attempt;
            public int x;
            public int y;
            public List<Sea.Coordinate> coordinates;

            public ComputerResult(Attempt a, int x, int y, List<Sea.Coordinate> coordinates)
            {
                this.attempt = a;
                this.x = x;
                this.y = y;
                this.coordinates = coordinates;
            }
        }

        private int[] generateVector()
        {
            int z;
            int[] vector = new int[Sea.VECTOR_SIZE];

            for (int i = 0; i < Sea.VECTOR_SIZE; i++)
            {
                do
                {
                    z = rnd.Next(Sea.VECTOR_SIZE + 1);
                }
                while (vector.Contains(z));
                vector[i] = z;
            }
            return vector;
        }

        public bool checkWhoWon(Player sth)
        {
            if (sth == Player.Player)
            {               
                return player.checkIfAllShipSank();
            }
            else if (sth == Player.Computer)
            {
                return computer.checkIfAllShipSank();
            }
            return false;
        }

        public void updateStatsForUser(bool wins)
        {
            SqlConnection con = new SqlConnection(Properties.Settings.Default.datasource);
            String query = "Update[batlleShip].[dbo].[user] set ";

            if (wins)
            {
                query += "Wins = Wins + 1";
            }
            else
            {
                query += "Losses = Losses + 1";
            }
            query += " Where UserName = @user";

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@user", username);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public double getWinningRate()
        {
            SqlConnection con = new SqlConnection(Properties.Settings.Default.datasource);

            SqlCommand cmd = new SqlCommand("Select Wins as w, Losses as ls from [batlleShip].[dbo].[user] where UserName=@username", con);
            cmd.Parameters.AddWithValue("@username", username);
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            con.Close();
            int wins = ds.Tables[0].Rows[0].Field<int>("w");
            int losses = ds.Tables[0].Rows[0].Field<int>("ls");

            if(wins+losses == 0)
            {
                return 0;
            }

            return 100*((double)wins/(wins + losses));
        }

    }
}



