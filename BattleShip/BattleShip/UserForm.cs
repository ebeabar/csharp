﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleShip
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();
        }

        private void login_Click(object sender, EventArgs e)
        {
            string user = username.Text;
            string passwd = password.Text;
            validation.Text = string.Empty;

            if (user == null || user == string.Empty || user.Length < 4)
            {
                validation.Text = "Username can't be empty or consist less than 4 characters";
                return;
            }
            if (passwd == null || passwd == string.Empty || passwd.Length < 4)
            {
                validation.Text = "Password can't be empty or consist less than 8 characters";
                return;
            }

            SqlConnection con = new SqlConnection(Properties.Settings.Default.datasource);
            SqlCommand cmd = new SqlCommand("Select * from [batlleShip].[dbo].[user] where UserName=@username and Password=@password", con);
            cmd.Parameters.AddWithValue("@username", user);
            cmd.Parameters.AddWithValue("@password", passwd);
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            con.Close();
            int count = ds.Tables[0].Rows.Count;

            if (count == 1)
            {
                Program.battleShip.initializeGame(user);
                ShipConfiguration shipConfiguration = new ShipConfiguration();
                shipConfiguration.Show();
                this.Hide();
            }
            else
            {
                validation.Text = "Bad credentials";
            }

        }

        private void createNewAccount_Click(object sender, EventArgs e)
        {
            UserCreation userCreation = new UserCreation();
            userCreation.Show();
            this.Hide();
        }

        private void helpButtonClicked(Object sender, CancelEventArgs e)
        {
            string text = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"help.txt"));
            System.Windows.Forms.MessageBox.Show(text, "Game Rules");
        }
    }
}
