﻿namespace BattleShip
{
    partial class UserCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username1 = new System.Windows.Forms.Label();
            this.password1 = new System.Windows.Forms.Label();
            this.repeatPassword1 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.repeatPassword = new System.Windows.Forms.TextBox();
            this.createUser = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.validation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // username1
            // 
            this.username1.AutoSize = true;
            this.username1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.username1.Location = new System.Drawing.Point(21, 41);
            this.username1.Name = "username1";
            this.username1.Size = new System.Drawing.Size(102, 25);
            this.username1.TabIndex = 0;
            this.username1.Text = "Username";
            // 
            // password1
            // 
            this.password1.AutoSize = true;
            this.password1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.password1.Location = new System.Drawing.Point(21, 75);
            this.password1.Name = "password1";
            this.password1.Size = new System.Drawing.Size(98, 25);
            this.password1.TabIndex = 1;
            this.password1.Text = "Password";
            // 
            // repeatPassword1
            // 
            this.repeatPassword1.AutoSize = true;
            this.repeatPassword1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.repeatPassword1.Location = new System.Drawing.Point(21, 108);
            this.repeatPassword1.Name = "repeatPassword1";
            this.repeatPassword1.Size = new System.Drawing.Size(163, 25);
            this.repeatPassword1.TabIndex = 2;
            this.repeatPassword1.Text = "Repeat password";
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.username.Location = new System.Drawing.Point(195, 36);
            this.username.MaxLength = 20;
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(318, 30);
            this.username.TabIndex = 3;
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.password.Location = new System.Drawing.Point(195, 72);
            this.password.MaxLength = 20;
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(318, 30);
            this.password.TabIndex = 4;
            // 
            // repeatPassword
            // 
            this.repeatPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.repeatPassword.Location = new System.Drawing.Point(195, 108);
            this.repeatPassword.MaxLength = 20;
            this.repeatPassword.Name = "repeatPassword";
            this.repeatPassword.PasswordChar = '*';
            this.repeatPassword.Size = new System.Drawing.Size(318, 30);
            this.repeatPassword.TabIndex = 5;
            // 
            // createUser
            // 
            this.createUser.Location = new System.Drawing.Point(410, 144);
            this.createUser.Name = "createUser";
            this.createUser.Size = new System.Drawing.Size(103, 30);
            this.createUser.TabIndex = 6;
            this.createUser.Text = "Create User";
            this.createUser.UseVisualStyleBackColor = true;
            this.createUser.Click += new System.EventHandler(this.createUser_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(438, 344);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(75, 33);
            this.back.TabIndex = 7;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // validation
            // 
            this.validation.AutoSize = true;
            this.validation.ForeColor = System.Drawing.Color.Red;
            this.validation.Location = new System.Drawing.Point(33, 197);
            this.validation.Name = "validation";
            this.validation.Size = new System.Drawing.Size(0, 17);
            this.validation.TabIndex = 8;
            // 
            // UserCreation
            // 
            this.AcceptButton = this.createUser;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 389);
            this.Controls.Add(this.validation);
            this.Controls.Add(this.back);
            this.Controls.Add(this.createUser);
            this.Controls.Add(this.repeatPassword);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Controls.Add(this.repeatPassword1);
            this.Controls.Add(this.password1);
            this.Controls.Add(this.username1);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserCreation";
            this.Text = "UserCreation";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.helpButtonClicked);
            this.Load += new System.EventHandler(this.UserCreation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label username1;
        private System.Windows.Forms.Label password1;
        private System.Windows.Forms.Label repeatPassword1;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox repeatPassword;
        private System.Windows.Forms.Button createUser;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label validation;
    }
}