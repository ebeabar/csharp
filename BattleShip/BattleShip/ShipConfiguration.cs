﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleShip
{
    public partial class ShipConfiguration : Form
    {
        private Button[,] playerButtons;
        private Button[,] computerButtons;
        int moves = 0;
        int ship = 0;

        private int[,] playersGame = new int[Sea.SEA_SIZE, Sea.SEA_SIZE];
        private int status = 4;

        private const int FOUR_1 = 1;
        private const int THREE_2 = 2;
        private const int TWO_3 = 3;
        private const int ONE_4 = 4;

        private int counter_1 = 0;
        private int counter_2 = 0;
        private int counter_3 = 0;
        private int counter_4 = 0;

        public enum CellStatus { Empty, Ship, Dmz, Hard };

        private List<Sea.Coordinate> coordinates = new List<Sea.Coordinate>();

        public ShipConfiguration()
        {
            InitializeComponent();
            playerButtons = new Button[Sea.SEA_SIZE, Sea.SEA_SIZE];
            computerButtons = new Button[Sea.SEA_SIZE, Sea.SEA_SIZE];
        }

        private void placeRows()
        {
            int[,] playerSea = Program.battleShip.player.SeaTable;
            int[,] playersHit = Program.battleShip.attemptPlayer;

            for (int r = 0; r < Sea.SEA_SIZE; r++)
            {
                createColumns(r, panelPlayer, playerButtons, playerSea, true);
                createColumns(r, panelComputer, computerButtons, playersHit, false);
            }
        }

        private void placeRowsForManualConfiguration()
        {
            labelGameStatus.Text = "Choose " + status + " masted ship";
            for (int r = 0; r < Sea.SEA_SIZE; r++)
            {
                int s = r * 25; //gap
                for (int c = 0; c < Sea.SEA_SIZE; c++)
                {
                    playerButtons[r, c] = new Button();
                    playerButtons[r, c].SetBounds(25 * c, s, 25, 25);
                    playerButtons[r, c].BackColor = System.Drawing.Color.White;
                    playerButtons[r, c].Click += new EventHandler(grid_Click2);

                    panelPlayer.Controls.Add(playerButtons[r, c]);
                }
            }
        }

        private void createColumns(int r, Panel panel, Button[,] buttons, int[,] table, bool disable)
        {
            int s = r * 25; //gap
            for (int c = 0; c < Sea.SEA_SIZE; c++)
            {
                buttons[r, c] = new Button();
                buttons[r, c].SetBounds(25 * c, s, 25, 25);
                buttons[r, c].Click += new EventHandler(grid_Click);
                if (disable)
                {
                    buttons[r, c].Enabled = false;
                }

                if (table[r, c] == 1)
                {
                    buttons[r, c].BackColor = System.Drawing.Color.Black;
                }
                else
                {
                    buttons[r, c].BackColor = System.Drawing.Color.White;
                }

                panel.Controls.Add(buttons[r, c]);
            }
        }

        public void grid_Click2(object sender, EventArgs e)
        {
            if (!(sender as Button).Enabled)
                return;
            int x = 0;
            int y = 0;
            Button button = sender as Button;

            for (int i = 0; i < playerButtons.GetLength(0); i++)
            {
                for (int j = 0; j < playerButtons.GetLength(1); j++)
                {
                    if (playerButtons[i, j] == button)
                    {
                        x = i;
                        y = j;
                        break;
                    }
                }
            }

            this.ActiveControl = labelGameStatus;

            button.Enabled = false;
            button.BackColor = Color.Black;
            playersGame[x, y] = (int)CellStatus.Ship;
            Sea.Coordinate shipCordinate = new Sea.Coordinate(x, y);
            coordinates.Add(shipCordinate);

            for (int i = 0; i < playerButtons.GetLength(0); i++)
            {
                for (int j = 0; j < playerButtons.GetLength(1); j++)
                {
                    int cellStatus = playersGame[i, j];
                    if (cellStatus == (int)CellStatus.Dmz)
                    {
                        playersGame[i, j] = (int)CellStatus.Empty;
                        playerButtons[i, j].BackColor = System.Drawing.Color.White;
                        playerButtons[i, j].Enabled = true;
                    }
                }
            }


            List<Sea.Coordinate> blankCoordinates = getAllGreyOut(coordinates);
            for (int i = 0; i < playerButtons.GetLength(0); i++)
            {
                for (int j = 0; j < playerButtons.GetLength(1); j++)
                {
                    int cellStatus = playersGame[i, j];

                    if (cellStatus == (int)CellStatus.Empty)
                    {
                        Sea.Coordinate coordianteTemp = new Sea.Coordinate(i, j);
                        if (!blankCoordinates.Contains(coordianteTemp))
                        {
                            playersGame[i, j] = (int)CellStatus.Dmz;
                            playerButtons[i, j].BackColor = System.Drawing.Color.Gray;
                            playerButtons[i, j].Enabled = false;
                        }
                    }
                }
            }

            if (status == coordinates.Count)
            {  
                
                Program.battleShip.applyUserConfig(coordinates, ship++);
                
                applyShip();
                coordinates.Clear();
                switch (status)
                {
                    case 1:
                        counter_1++;
                        if (counter_1 == 4)
                        {
                            labelGameStatus.Text = "";
                            panelPlayer.Controls.Clear();
                            playerButtons = new Button[Sea.SEA_SIZE, Sea.SEA_SIZE];
                            computerButtons = new Button[Sea.SEA_SIZE, Sea.SEA_SIZE];
                            BattleShip.Player firstPlayer = Program.battleShip.startGame();
                            placeRows();

                            if (firstPlayer == BattleShip.Player.Player)
                            {
                                executeThreeStepsComputer();
                            }
                        }
                        break;
                    case 2:
                        counter_2++;
                        if (counter_2 == 3)
                        {
                            status--;
                            labelGameStatus.Text = "Choose " + status + " masted ship";
                        }
                        break;
                    case 3:
                        counter_3++;
                        if (counter_3 == 2)
                        {
                            status--;
                            labelGameStatus.Text = "Choose " + status + " masted ship";
                        }
                        break;
                    case 4:
                        counter_4++;
                        if(counter_4 == 1)
                        {
                            status--;
                            labelGameStatus.Text = "Choose " + status + " masted ship";
                        }  
                        break;
                }
            }


        }

       private void applyShip()
        {
            for (int i = 0; i < playerButtons.GetLength(0); i++)
            {
                for (int j = 0; j < playerButtons.GetLength(1); j++)
                {
                    int cellStatus = playersGame[i, j];
                    if (cellStatus == (int)CellStatus.Dmz)
                    {
                        playersGame[i, j] = (int)CellStatus.Empty;
                        playerButtons[i, j].BackColor = System.Drawing.Color.White;
                        playerButtons[i, j].Enabled = true;
                    }
                }
            }

            List<Sea.Coordinate> blankCoordinates = getAllHard(coordinates);
            for (int i = 0; i < playerButtons.GetLength(0); i++)
            {
                for (int j = 0; j < playerButtons.GetLength(1); j++)
                {
                    int cellStatus = playersGame[i, j];

                    if (cellStatus == (int)CellStatus.Empty)
                    {
                        Sea.Coordinate coordianteTemp = new Sea.Coordinate(i, j);
                        if (blankCoordinates.Contains(coordianteTemp))
                        {
                            playersGame[i, j] = (int)CellStatus.Hard;
                            playerButtons[i, j].BackColor = System.Drawing.Color.Gray;
                            playerButtons[i, j].Enabled = false;
                        }
                    }
                }
            }
        }

        private List<Sea.Coordinate> getAllGreyOut(List<Sea.Coordinate> coordinates)
        {
            List<Sea.Coordinate> result = new List<Sea.Coordinate>();
            foreach (Sea.Coordinate cordinate in coordinates)
            {
                result.AddRange(greyOutAllAdjacent(cordinate.x, cordinate.y));
            }
            return result;
        }

        private List<Sea.Coordinate> greyOutAllAdjacent(int x, int y)
        {
            List<Sea.Coordinate> result = new List<Sea.Coordinate>();

            Sea.Coordinate shiftCoordinateRight = new Sea.Coordinate(x, y + 1);
            Sea.Coordinate shiftCoordinateLeft = new Sea.Coordinate(x, y - 1);
            Sea.Coordinate shiftCoordinateUp = new Sea.Coordinate(x + 1, y);
            Sea.Coordinate shiftCoordinateDown = new Sea.Coordinate(x - 1, y);

            List<Sea.Coordinate> coordinates = new List<Sea.Coordinate>(4);
            coordinates.Add(shiftCoordinateRight);
            coordinates.Add(shiftCoordinateLeft);
            coordinates.Add(shiftCoordinateUp);
            coordinates.Add(shiftCoordinateDown);

            foreach (Sea.Coordinate coord in coordinates)
            {
                if (checkIfExists(coord))
                {
                    result.Add(coord);
                }
            }
            return result;
        }

        private List<Sea.Coordinate> getAllHard(List<Sea.Coordinate> coordinates)
        {
            List<Sea.Coordinate> result = new List<Sea.Coordinate>();
            foreach (Sea.Coordinate cordinate in coordinates)
            {
                result.AddRange(hardAllAdjacent(cordinate));
            }
            return result;
        }

        private List<Sea.Coordinate> hardAllAdjacent(Sea.Coordinate coordinate)
        {
            List<Sea.Coordinate> result = new List<Sea.Coordinate>();

            Sea.Coordinate shiftCoordinateRight = new Sea.Coordinate(coordinate.x, coordinate.y + 1);
            Sea.Coordinate shiftCoordinateLeft = new Sea.Coordinate(coordinate.x, coordinate.y - 1);
            Sea.Coordinate shiftCoordinateUp = new Sea.Coordinate(coordinate.x + 1, coordinate.y);
            Sea.Coordinate shiftCoordinateDown = new Sea.Coordinate(coordinate.x - 1, coordinate.y);
            Sea.Coordinate shiftCoordinateUpRight = new Sea.Coordinate(coordinate.x + 1, coordinate.y + 1);
            Sea.Coordinate shiftCoordinateUpLeft = new Sea.Coordinate(coordinate.x + 1, coordinate.y - 1);
            Sea.Coordinate shiftCoordinateDownLeft = new Sea.Coordinate(coordinate.x - 1, coordinate.y - 1);
            Sea.Coordinate shiftCoordinateDownRight = new Sea.Coordinate(coordinate.x - 1, coordinate.y + 1);

            List<Sea.Coordinate> coordinates = new List<Sea.Coordinate>(8);
            coordinates.Add(shiftCoordinateRight);
            coordinates.Add(shiftCoordinateLeft);
            coordinates.Add(shiftCoordinateUp);
            coordinates.Add(shiftCoordinateDown);
            coordinates.Add(shiftCoordinateUpRight);
            coordinates.Add(shiftCoordinateUpLeft);
            coordinates.Add(shiftCoordinateDownLeft);
            coordinates.Add(shiftCoordinateDownRight);

            foreach (Sea.Coordinate coord in coordinates)
            {
                if (isInBounds(coord.x, coord.y))
                {
                    result.Add(coord);
                }
            }
            return result;
        }

        //check if cell is in the Sea
        public bool isInBounds(int x, int y)
        {
            if ((x < Sea.SEA_SIZE && x >= 0) && (y < Sea.SEA_SIZE && y >= 0))
            {
                return true;
            }
            return false;
        }

        //check if cell is "white"/empty
        private bool isEmpty(int x, int y)
        {
            if (playersGame[x, y] == (int)CellStatus.Empty)
            {
                return true;
            }
            return false;
        }


        //check if coordinate exist in the Sea
        private bool checkIfExists(Sea.Coordinate coordinate)
        {
            if (isInBounds(coordinate.x, coordinate.y) && isEmpty(coordinate.x, coordinate.y))
            {
                return true;
            }
            return false;
        }

        //backcolor change
        public void grid_Click(object sender, EventArgs e)
        {
            if (!(sender as Button).Enabled)
                return;
            int x = 0;
            int y = 0;
            Button button = sender as Button;

            for (int i = 0; i < computerButtons.GetLength(0); i++)
            {
                for (int j = 0; j < computerButtons.GetLength(1); j++)
                {
                    if (computerButtons[i, j] == button)
                    {
                        x = i;
                        y = j;
                        break;
                    }
                }
            }

            Sea.CoordrinateStatusHit coordrinateStatusHit = Program.battleShip.playerGame(x, y);
            BattleShip.Attempt attempt = coordrinateStatusHit.attempt;

            if (attempt == BattleShip.Attempt.Blank)
            {
                button.BackColor = Color.Gray;
                labelMessage("Blank!");
            }
            else if (attempt == BattleShip.Attempt.Hit)
            {
                button.BackColor = Color.Black;
                labelMessage("Hit!");
            }
            else if (attempt == BattleShip.Attempt.Sank)
            {
                button.BackColor = Color.Black;
                labelMessage("Sank!");

                foreach (Sea.Coordinate coordinate in coordrinateStatusHit.coordinates)
                {
                    List<Sea.Coordinate> blankCoordinates = Program.battleShip.greyOutAllAdjacent(coordinate.x, coordinate.y, true);
                    foreach(Sea.Coordinate blankCoordinate in blankCoordinates)
                    {
                        computerButtons[blankCoordinate.x, blankCoordinate.y].BackColor = Color.Gray;
                    }
                }
            }

            this.ActiveControl = labelGameStatus;
            button.Enabled = false;


            bool result = Program.battleShip.checkWhoWon(BattleShip.Player.Computer);
            if (result)
            {
                Program.battleShip.updateStatsForUser(true);
                double wins = Program.battleShip.getWinningRate();

                MessageBox.Show("You won! \n Your winning rate is " + Math.Round(wins,2) + "%"); 
                Application.Exit();
            }

            moves++;

            if (moves == 3)
            {
                executeThreeStepsComputer();
                moves = 0;
            }

        }  

        private void executeThreeStepsComputer()
        {
            for (int i = 0; i < 3; i++)
            {
                BattleShip.ComputerResult computerResult = Program.battleShip.computerGame();
                BattleShip.Attempt attempt = computerResult.attempt;
                Button button = playerButtons[computerResult.x, computerResult.y];

                if (attempt == BattleShip.Attempt.Blank)
                {
                    button.BackColor = Color.Gray;
                }
                else if (attempt == BattleShip.Attempt.Hit)
                {
                    button.BackColor = Color.Red;
                }
                else if (attempt == BattleShip.Attempt.Sank)
                {
                    button.BackColor = Color.Red;

                    foreach (Sea.Coordinate coordinate in computerResult.coordinates)
                    {
                        List<Sea.Coordinate> blankCoordinates = Program.battleShip.greyOutAllAdjacent(coordinate.x, coordinate.y, false);
                        foreach (Sea.Coordinate blankCoordinate in blankCoordinates)
                        {
                            playerButtons[blankCoordinate.x, blankCoordinate.y].BackColor = Color.Gray;
                        }
                    }
                }

                bool result = Program.battleShip.checkWhoWon(BattleShip.Player.Player);
                if (result)
                {
                    Program.battleShip.updateStatsForUser(false);
                    double wins = Program.battleShip.getWinningRate();

                    MessageBox.Show("You lost! \n Your winning rate is " + Math.Round(wins,2) + "%");
                    Application.Exit();
                }

            }
        }

        public void labelMessage(String message)
        {
            labelGameStatus.Text = message;
        }

        private void ShipConfiguration_Load(object sender, EventArgs e)
        {
            this.ActiveControl = labelGameStatus;
            DialogResult result = MessageBox.Show("Do you want to generate ships randomly?", "Ship Genetation", MessageBoxButtons.YesNo);

            // Process message box results
            switch (result)
            {
                case DialogResult.Yes:
                    //startGameButton.Visible = false;
                    Program.battleShip.generateShipConfiguration();
                    BattleShip.Player firstPlayer = Program.battleShip.startGame();
                    placeRows();

                    if (firstPlayer == BattleShip.Player.Player)
                    {
                        executeThreeStepsComputer();
                    }
                    break;
                case DialogResult.No:
                    placeRowsForManualConfiguration();
                    break;
            }

        }

        private void helpButtonClicked(Object sender, CancelEventArgs e)
        {
            string text = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"help.txt"));
            System.Windows.Forms.MessageBox.Show(text, "Game Rules");
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }

       /* private void startGameButton_Click(object sender, EventArgs e)
        {
           // startGameButton.Visible = false;
            BattleShip.Player firstPlayer = Program.battleShip.startGame();
            placeRows();

            if (firstPlayer == BattleShip.Player.Player)
            {
                executeThreeStepsComputer();
            }
        }*/

        private void winningRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double wins = Program.battleShip.getWinningRate();

            MessageBox.Show("Your winning rate is " + Math.Round(wins, 2) + "%");
        }
    }


}
